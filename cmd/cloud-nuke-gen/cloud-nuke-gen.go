package main

import (
	"log"
	"os"

	"gitlab.com/m4573rh4ck3r/cloud-nuke-gen/internal/gen"
)

func logErr(err error) {
	log.SetFlags(log.Lshortfile)
	log.SetOutput(os.Stderr)
	log.Fatalln(err)
}

func main() {
	if err := gen.MainPkg(); err != nil {
		logErr(err)
	}
	if err := gen.LogPkg(); err != nil {
		logErr(err)
	}
	if err := gen.Commands(); err != nil {
		logErr(err)
	}
	if err := gen.Resources(); err != nil {
		logErr(err)
	}
	if err := gen.Services(); err != nil {
		logErr(err)
	}
	if err := gen.VersionPkg(); err != nil {
		logErr(err)
	}
}
