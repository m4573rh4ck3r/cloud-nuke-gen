SHELL:=/usr/bin/env bash

project_name:=cloud-nuke-gen
repository:=gitlab.com/m4573rh4ck3r/cloud-nuke-gen
main_module:=$(repository)/cmd
git_commit_sha:=$(shell git log --format="%h" -n1)
git_branch=$(shell git branch --show-current)
coverage_file:=.testCoverage.txt
version:=$(shell git describe --exact-match --tags $(git_commit_sha) 2>/dev/null | sed 's/^v//g')
build_time=$(shell date +'%Y-%m-%d_%T')
srcdir:=cmd
prefix:=/usr/local
exec_prefix:=$(prefix)
bindir:=$(exec_prefix)/bin
testdir:=./...
builddir:=$(CURDIR)/bin
distdir:=_dist
INSTALL:=$(CC) install
INSTALL_PROGRAM:=$(INSTALL)
DESTDIR=

PACKAGE_REGISTRY_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/cloud-nuke-gen/${PACKAGE_VERSION}"

TARGETS:=darwin/amd64 darwin/arm64 linux/amd64 linux/arm linux/arm64 windows/amd64

TARGET_OBJS ?= darwin-amd64.tar.gz darwin-amd64.tar.gz.sha256 darwin-amd64.tar.gz.sha256sum darwin-arm64.tar.gz darwin-arm64.tar.gz.sha256 darwin-arm64.tar.gz.sha256sum linux-amd64.tar.gz linux-amd64.tar.gz.sha256 linux-amd64.tar.gz.sha256sum linux-arm.tar.gz linux-arm.tar.gz.sha256 linux-arm.tar.gz.sha256sum linux-arm64.tar.gz linux-arm64.tar.gz.sha256 linux-arm64.tar.gz.sha256sum windows-amd64.zip windows-amd64.zip.sha256 windows-amd64.zip.sha256sum

ifeq ($(version),)
	version=unstable
endif

ifeq ($(CC),cc)
	CC=go
endif

ifeq ($(buildflags),)
	buildflags = -v
endif

ifeq ($(ldflags),)
	ldflags += -w -s
	ldflags += -X '$(repository)/internal/version.Version=$(version)'
	ldflags += -X '$(repository)/internal/version.GitRevision=$(git_commit_sha)'
	ldflags += -X '$(repository)/internal/version.GitBranch=$(git_branch)'
	ldflags += -X '$(repository)/internal/version.BuildTime=$(build_time)'
endif

.SUFFIXES:
.SUFFIXES: .go .tf .yaml

objects=$(builddir)/$(project_name) $(builddir)/gen

.PHONY: all
all: clean build ## compile the objects for the current os/arch

$(builddir):
	@mkdir -p $@

$(builddir)/%: | $(builddir)

# $(objects): | $(builddir)
	# @$(foreach object,$(objects),$(CC) build $(buildflags) -ldflags "$(ldflags)" -o $(builddir)/$(notdir $(object)) $(srcdir)/$(notdir $(object))/$(notdir $(object)).go;)

$(distdir):
	@mkdir -p $@

$(distdirs): $(distdir)
	@$(foreach target,$(TARGETS),mkdir -p $(distdir)/$(subst /,-,$(target));)

$(TARGETS): $(distdirs)
	@$(foreach target,$(TARGETS), CGO_ENABLED=0 GOOS=$(word 1,$(subst /, ,$(target))) GOARCH=$(word 2,$(subst /, ,$(target))) $(CC) build $(buildflags) -ldflags "$(ldflags)" -o $(distdir)/$(subst /,-,$(target))/$(project_name) $(main_module)/$(project_name);)

$(builddir)/golint:
	GO111MODULE=off GOBIN=$(builddir) go get -u golang.org/x/lint/golint

.PHONY: build
build: $(builddir) ## compile the binary for the current os/arch
	@$(CC) build $(buildflags) -ldflags "$(ldflags)" -o /tmp/$(project_name) $(srcdir)/$(project_name)/$(project_name).go
	@/tmp/$(project_name)
	@set -eux; \
		mkdir -p _generated/internal && \
		cd $${cloud_nuke_gen_prefix?} && \
		test -f "go.mod" || go mod init $(repository) && \
		go mod tidy && \
		go build $(buildflags) -ldflags "$(ldflags)" -o $(builddir)/$(project_name) $(srcdir)/$(project_name)/$(project_name).go

.PHONY: build-cross
build-cross: ldflags += -extldflags "-static"
build-cross: $(TARGETS) ## cross compile for all os/arch's

.PHONY: lint
lint: | $(builddir)/golint ## run golint
	@$(builddir)/golint -set_exit_status ./...

.PHONY: dist
dist: ## create distribution archives for all cross-compiled binaries
	@( \
		cd $(distdir) && \
		$(foreach target,$(TARGETS),tar -zcf $(project_name)-$(version)-$(subst /,-,$(target)).tar.gz $(subst /,-,$(target));) \
		$(foreach target,$(TARGETS),zip -r $(project_name)-$(version)-$(subst /,-,$(target)).zip $(subst /,-,$(target));) \
	)

.PHONY: checksum
checksum: ## generate checksums for created distribution archives
	@for f in $$(ls _dist/*.{gz,zip} 2>/dev/null) ; do \
		shasum -a 256 "$${f}" | sed 's/_dist\///' > "$${f}.sha256sum" ; \
		shasum -a 256 "$${f}" | awk '{print $$1}' > "$${f}.sha256" ; \
	done

.PHONY: sign
sign: ## sign all distribution archives and their checksum files
	@for f in $$(ls _dist/*.{gz,zip,sha256,sha256sum} 2>/dev/null) ; do \
		gpg --armor --detach-sign $${f} ; \
	done

.PHONY: distclean
distclean: ## cleanup build dependencies
	@rm -rvf $(builddir)
	@rm -rvf $(distdir)
	@rm -rvf _generated
	@rm -rvf pkg
	@$(CC) clean -x -cache

.PHONY: mod-clean
mod-clean: ## cleanup mod dependencies
	@$(CC) clean -x -modcache

.PHONY: test-clean
test-clean: ## cleanup test cache
	@$(CC) clean -x -testcache

.PHONY: clean
clean: distclean mod-clean test-clean ## remove the entire cache

.PHONY: test
test: ## run tests
	@$(CC) test -parallel 6 -v $(testdir) -run Test* | sed -e '/PASS/ s//$(shell printf "\033[32mPASS\033[0m")/' -e '/FAIL/ s//$(shell printf "\033[31mFAIL\033[0m")/' -e '/SKIP/ s//$(shell printf "\033[93mSKIP\033[0m")/'

.PHONY: bench
bench: ## run benchmarks
	@for t in $$(find pkg/gcp -mindepth 1 -maxdepth 1 -type d) ; do \
		(cd $${t} ; \
		go test -bench=. -run Benchmark*) ; \
	done

.PHONY: race
race: ## run tests with race detection
	@$(CC) test -parallel 6 -v -race $(testdir)

.PHONY: coverage
coverage: ## run tests with coverage
	@$(CC) test -parallel 6 -v -cover $(testdir)

.PHONY: fmt
fmt: ## format all files
	@gofmt -l -w -s .
	@goimports -w -format-only -local "$(repository)" .

.PHONY: get
get: ## get dependencies
	@$(CC) mod download

.PHONY: update-deps
update-deps: ## update dependencies
	@$(CC) get -u ./...

.PHONY: tidy
tidy: ## tidy up dependencies
	@$(CC) mod tidy

.PHONY: check-fmt
check-fmt: ## check if all files are formatted correctly
	@hack/check-fmt.sh

.PHONY: install
install: ## copy the binary into $(DESTDIR)$(bindir)
	@$(foreach object,$(objects), install -D -t $(DESTDIR)$(bindir) $(builddir)/$(notdir $(object));)

.PHONY: uninstall
uninstall: ## remove the binary from $(DESTDIR)$(bindir)
	@$(foreach object,$(objects), rm -vf $(DESTDIR)$(bindir)/$(notdir $(object));)

.PHONY: release-major
release-major: check-fmt ## increase the major version
	@inc=major hack/releasae.sh

.PHONY: release-minor
release-minor: check-fmt ## increase the minor version
	@inc=minor hack/releasae.sh

.PHONY: release-patch
release-patch: check-fmt ## increase the patch version
	@inc=patch hack/releasae.sh

.PHONY: upload
upload: ## upload the distribution archives to gitlab
	@for f in $$(cd $(distdir) && ls *.{gz,zip,gz.sha256,gz.sha256sum,zip.sha256,zip.sha256sum} 2>/dev/null) ; do \
		curl --header \"JOB-TOKEN: ${CI_JOB_TOKEN}\" --upload-file _dist/$${f} ${PACKAGE_REGISTRY_URL}/$${f} ; \
	done

.PHONY: release
release: ## create a new gitlab release
	@for f in $$(cd $(distdir) && ls *.{gz,zip,gz.sha256,gz.sha256sum,zip.sha256,zip.sha256sum} 2>/dev/null) ; do \
		release-cli create --name "Release $$CI_COMMIT_TAG" --tag-name $$CI_COMMIT_TAG \
			--assets-link "{\"name\":\"$${f}\",\"url\":\"${PACKAGE_REGISTRY_URL}/$${f}\"}" ; \
	done

.PHONY: info
info: ## display this info
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<configurations> <target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "   \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
