package version

import (
	"fmt"
	"runtime"
)

var (
	// Version is the applications release version
	Version = ""
	// GitRevision is the latest commit hash
	GitRevision = ""
	// GitBranch is the branch from which the application was built
	GitBranch = ""
	// GoVersion is the golang version that was used to build the application
	GoVersion = runtime.Version()
	// OsArch is the operating system and architecture the application was built for
	OsArch = fmt.Sprintf("%s/%s", runtime.GOOS, runtime.GOARCH)
	// BuildTime is the time at which the application was built
	BuildTime = ""
)

// DetailedVersionTemplate is the template to use for parsing the detailed version output
var DetailedVersionTemplate = `%s
Version:      %s
Git revision: %s
Git branch:   %s
Go version:   %s
Build time:   %s
OS/Arch:      %s
`
