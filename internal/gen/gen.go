package gen

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"

	"gopkg.in/yaml.v2"
)

func LogPkg() error {
	return parseLog(&Log{Name: "log", TemplateFile: "tmpl/log.tmpl"})
}

// MainPkg generates the main package
func MainPkg() error {
	return parseMain(&Main{Name: "cloud-nuke-gen", TemplateFile: "tmpl/main.tmpl"})
}

func VersionPkg() error {
	return parseVersion(&Version{Name: "version", TemplateFile: "tmpl/version.tmpl"})
}

// Commands generates the command line options package
func Commands() error {
	resources, err := getResources()
	if err != nil {
		return err
	}

	services, err := getServices()
	if err != nil {
		return err
	}

	c := &GcpCmd{
		Services:     services,
		Resources:    resources,
		Name:         "cmd",
		TemplateFile: "tmpl/gcpCmd.tmpl",
	}

	if err := parseGcpCmd(c); err != nil {
		return err
	}

	cmds := []*GenericCmd{
		{Name: "root", TemplateFile: "tmpl/rootCmd.tmpl"},
		{Name: "completion", TemplateFile: "tmpl/completionCmd.tmpl"},
		{Name: "version", TemplateFile: "tmpl/versionCmd.tmpl"},
		{Name: "root", TemplateFile: "tmpl/rootCmd.tmpl"},
	}

	for _, cmd := range cmds {
		if err := parseCmd(cmd); err != nil {
			return err
		}
	}

	return nil
}

func getServicesConfig() (*map[string]map[string]map[string]string, error) {
	var config map[string]map[string]map[string]string
	yamlFile, err := ioutil.ReadFile("configs/services.yaml")
	if err != nil {
		return nil, err
	}
	if err := yaml.Unmarshal(yamlFile, &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func getResourcesConfig() (*map[string]map[string]map[string]map[string]string, error) {
	var config map[string]map[string]map[string]map[string]string
	yamlFile, err := ioutil.ReadFile("configs/resources.yaml")
	if err != nil {
		return nil, err
	}
	if err := yaml.Unmarshal(yamlFile, &config); err != nil {
		return nil, err
	}
	return &config, nil
}

func getResources() ([]*Resource, error) {
	var resources []*Resource
	var resourceNameConcatenated string
	config, err := getResourcesConfig()
	if err != nil {
		return nil, err
	}
	for providerName, provider := range *config {
		for pkgName, pkg := range provider {
			for resourceName, res := range pkg {
				resourceNameConcatenated = resourceName
				if regexp.MustCompile(`.*_{1}.*`).MatchString(resourceName) {
					strSplit := strings.Split(resourceName, "_")
					resourceNameConcatenated = strSplit[0]
					for _, part := range strSplit {
						if part == strSplit[0] {
							continue
						}
						resourceNameConcatenated += strings.Title(part)
					}
				}

				gcpResourceName := resourceNameConcatenated

				if regexp.MustCompile(`^global.*`).MatchString(resourceNameConcatenated) {
					gcpResourceName = strings.ReplaceAll(resourceNameConcatenated, "global", "")
				}

				r := &Resource{
					Name:             resourceNameConcatenated,
					Package:          pkgName,
					Provider:         providerName,
					Description:      fmt.Sprintf("%s %s", pkgName, strings.ReplaceAll(resourceName, "_", " ")),
					FullName:         fmt.Sprintf("%s%s", pkgName, strings.Title(resourceNameConcatenated)),
					GetFuncName:      fmt.Sprintf("get%s%s", strings.Title(pkgName), pluralize(strings.Title(resourceNameConcatenated))),
					DeleteFuncName:   fmt.Sprintf("Delete%s%s", strings.Title(pkgName), pluralize(strings.Title(resourceNameConcatenated))),
					TemplateFile:     fmt.Sprintf("tmpl/%s.tmpl", res["tmpl"]),
					TestTemplateFile: fmt.Sprintf("tmpl/%s_test.tmpl", res["tmpl"]),
					GCPResourceName:  gcpResourceName,
					APIVersion:       res["apiVersion"],
				}
				resources = append(resources, r)
			}
		}
	}
	return resources, nil
}

// Resources iterates over all resources defined in configs/resources.yaml and passes them to parseResource which executes them with their associated template file
func Resources() error {
	resources, err := getResources()
	if err != nil {
		return err
	}

	for _, r := range resources {
		if err := parseResource(r); err != nil {
			return err
		}
	}

	return nil
}

func getServices() ([]*Service, error) {
	var services []*Service
	config, err := getServicesConfig()
	if err != nil {
		return nil, err
	}

	for providerName, provider := range *config {
		for pkgName, pkg := range provider {
			if regexp.MustCompile(`.*_.*`).MatchString(pkgName) {
				strSplit := strings.Split(pkgName, "_")
				pkgName = fmt.Sprintf("%s%s", strSplit[0], strings.Title(strSplit[1]))
			}

			s := &Service{
				Name:             pkgName,
				Provider:         providerName,
				Description:      fmt.Sprintf("%s service", pkgName),
				FullName:         fmt.Sprintf("%sService", pkgName),
				GetFuncName:      fmt.Sprintf("get%sService", strings.Title(pkgName)),
				TemplateFile:     fmt.Sprintf("tmpl/%s.tmpl", pkg["tmpl"]),
				TestTemplateFile: fmt.Sprintf("tmpl/%s_test.tmpl", pkg["tmpl"]),
				APIVersion:       pkg["apiVersion"],
			}
			services = append(services, s)
		}
	}

	return services, nil
}

// Services iterates over all services defined in configs/services.yaml and passes them to parseService which executes them with their associated template file
func Services() error {
	services, err := getServices()
	if err != nil {
		return err
	}

	for _, s := range services {
		if err := parseService(s); err != nil {
			return err
		}
	}

	return nil
}
