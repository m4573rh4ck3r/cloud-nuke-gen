package gen

import (
	"errors"
	"io"
	"log"
	"os"
	"path/filepath"
	"text/template"
)

func parseCmd(c *GenericCmd) error {
	dir := filepath.Join(os.Getenv("cloud_nuke_gen_prefix"), "internal", "cmd")
	_, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	if os.IsNotExist(err) {
		os.MkdirAll(dir, os.ModePerm)
	}

	f := filepath.Join(dir, c.Name+".go")

	if f == "" {
		return errors.New("failed to create filepath")
	}

	goFile, err := os.OpenFile(f, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return err
	}

	writer := io.MultiWriter(goFile)
	tmpl := template.Must(template.ParseFiles(c.TemplateFile))
	if err := tmpl.Execute(writer, c); err != nil {
		return err
	}

	return nil
}

func parseGcpCmd(c *GcpCmd) error {
	dir := filepath.Join(os.Getenv("cloud_nuke_gen_prefix"), "internal", "cmd")
	_, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	if os.IsNotExist(err) {
		os.MkdirAll(dir, os.ModePerm)
	}

	f := filepath.Join(dir, c.Name+".go")

	if f == "" {
		return errors.New("failed to create filepath")
	}

	goFile, err := os.OpenFile(f, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return err
	}

	writer := io.MultiWriter(goFile)
	tmpl := template.Must(template.ParseFiles(c.TemplateFile))
	if err := tmpl.Execute(writer, c); err != nil {
		return err
	}

	return nil
}

func parseLog(l *Log) error {
	dir := filepath.Join(os.Getenv("cloud_nuke_gen_prefix"), "internal", l.Name)
	_, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	if os.IsNotExist(err) {
		os.MkdirAll(dir, os.ModePerm)
	}

	log.Printf("created directory %s\n", dir)

	f := filepath.Join(dir, l.Name+".go")
	if f == "" {
		return errors.New("failed to create filepath")
	}

	goFile, err := os.OpenFile(f, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return err
	}

	writer := io.MultiWriter(goFile)
	tmpl := template.Must(template.ParseFiles(l.TemplateFile))
	if err := tmpl.Execute(writer, l); err != nil {
		return err
	}

	return nil
}

func parseVersion(v *Version) error {
	dir := filepath.Join(os.Getenv("cloud_nuke_gen_prefix"), "internal", v.Name)
	_, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	if os.IsNotExist(err) {
		os.MkdirAll(dir, os.ModePerm)
	}

	log.Printf("created directory %s\n", dir)

	f := filepath.Join(dir, v.Name+".go")
	if f == "" {
		return errors.New("failed to create filepath")
	}

	goFile, err := os.OpenFile(f, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return err
	}

	writer := io.MultiWriter(goFile)
	tmpl := template.Must(template.ParseFiles(v.TemplateFile))
	if err := tmpl.Execute(writer, v); err != nil {
		return err
	}

	return nil
}

// parseMain parses the applications main package template file
func parseMain(m *Main) error {
	dir := filepath.Join(os.Getenv("cloud_nuke_gen_prefix"), "cmd", m.Name)
	_, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	if os.IsNotExist(err) {
		os.MkdirAll(dir, os.ModePerm)
	}

	log.Printf("created directory %s\n", dir)

	f := filepath.Join(dir, m.Name+".go")
	if f == "" {
		return errors.New("failed to create filepath")
	}

	goFile, err := os.OpenFile(f, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return err
	}

	writer := io.MultiWriter(goFile)
	tmpl := template.Must(template.ParseFiles(m.TemplateFile))
	if err := tmpl.Execute(writer, m); err != nil {
		return err
	}

	return nil
}

func parseResource(r *Resource) error {
	dir := filepath.Join(os.Getenv("cloud_nuke_gen_prefix"), "pkg", r.Provider, r.Package)
	_, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	if os.IsNotExist(err) {
		os.MkdirAll(dir, os.ModePerm)
	}

	log.Printf("created directory %s\n", dir)

	f := filepath.Join(dir, r.Pluralize(r.Name)+".go")

	if f == "" {
		return errors.New("failed to create filepath")
	}

	goFile, err := os.OpenFile(f, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return err
	}

	writer := io.MultiWriter(goFile)
	tmpl := template.Must(template.ParseFiles(r.TemplateFile))
	if err := tmpl.Execute(writer, r); err != nil {
		return err
	}
	log.Printf("successfully created go file %s\n", goFile.Name())

	if r.TemplateFile == "tmpl/simple.tmpl" || r.TemplateFile == "tmpl/simpleWithRegion.tmpl" {
		fTest := filepath.Join(dir, r.Pluralize(r.Name)+"_test.go")
		goTestFile, err := os.OpenFile(fTest, os.O_CREATE|os.O_RDWR, 0666)
		if err != nil {
			return err
		}

		testWriter := io.MultiWriter(goTestFile)
		testTmpl := template.Must(template.ParseFiles(r.TestTemplateFile))
		if err := testTmpl.Execute(testWriter, r); err != nil {
			return err
		}
		log.Printf("successfully created go test file %s\n", goTestFile.Name())
	}

	return nil
}

func parseService(s *Service) error {
	dir := filepath.Join(os.Getenv("cloud_nuke_gen_prefix"), "pkg", s.Provider, s.Name)
	_, err := os.Stat(dir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	if os.IsNotExist(err) {
		os.MkdirAll(dir, os.ModePerm)
	}

	f := filepath.Join(dir, s.Name+".go")

	if f == "" {
		return errors.New("failed to create filepath")
	}

	goFile, err := os.OpenFile(f, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return err
	}

	writer := io.MultiWriter(goFile)
	tmpl := template.Must(template.ParseFiles(s.TemplateFile))
	if err := tmpl.Execute(writer, s); err != nil {
		return err
	}

	fTest := filepath.Join(dir, s.Name+"_test.go")
	goTestFile, err := os.OpenFile(fTest, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return err
	}

	testWriter := io.MultiWriter(goTestFile)
	testTmpl := template.Must(template.ParseFiles(s.TestTemplateFile))
	if err := testTmpl.Execute(testWriter, s); err != nil {
		return err
	}
	log.Printf("successfully created go test file %s\n", goTestFile.Name())

	return nil
}
