package gen

import (
	"fmt"
	"regexp"
	"unicode"
	"unicode/utf8"
)

type Log struct {
	Name         string
	TemplateFile string
}

type GcpCmd struct {
	Name         string
	TemplateFile string
	Resources    []*Resource
	Services     []*Service
}

type GenericCmd struct {
	Name         string
	TemplateFile string
}

type Main struct {
	Name         string
	TemplateFile string
}

type Version struct {
	Name         string
	TemplateFile string
}

// Resource contains all information required to parse a resource template file
type Resource struct {
	Name             string
	FullName         string
	Package          string
	Provider         string
	Description      string
	GetFuncName      string
	DeleteFuncName   string
	TemplateFile     string
	TestTemplateFile string
	GCPResourceName  string
	APIVersion       string
}

// Capitalize returns the provided string with the first letter transformed into uppercase
func (r *Resource) Capitalize(name string) string {
	return capitalize(name)
}

// Pluralize return the provided string with the last word of it transformed into it' plural form
func (r *Resource) Pluralize(name string) string {
	return pluralize(name)
}

// PluralizeCapitalization returns the provided string with the first letter transformed into uppercase and the last word of it transformed into it's plural form
func (r *Resource) PluralizeCapitalization(name string) string {
	return pluralizeCapitalization(name)
}

// Service contains all information required to parse a resource template file
type Service struct {
	Name             string
	Provider         string
	Description      string
	FullName         string
	GetFuncName      string
	TemplateFile     string
	TestTemplateFile string
	APIVersion       string
}

// Capitalize returns the provided string with the first letter transformed into uppercase
func (s *Service) Capitalize(name string) string {
	return capitalize(name)
}

// Pluralize return the provided string with the last word of it transformed into it' plural form
func (s *Service) Pluralize(name string) string {
	return pluralize(name)
}

// PluralizeCapitalization returns the provided string with the first letter transformed into uppercase and the last word of it transformed into it's plural form
func (s *Service) PluralizeCapitalization(name string) string {
	return pluralizeCapitalization(name)
}

// Pluralize return the provided string with the last word of it transformed into it' plural form
func pluralize(name string) string {
	if regexp.MustCompile(`.*s$`).MatchString(name) {
		return fmt.Sprintf("%ses", name)
	}
	return fmt.Sprintf("%ss", name)
}

// Capitalize returns the provided string with the first letter transformed into uppercase
func capitalize(name string) string {
	if name == "" {
		return ""
	}
	r, n := utf8.DecodeRuneInString(name)
	return string(unicode.ToUpper(r)) + name[n:]
}

// PluralizeCapitalization returns the provided string with the first letter transformed into uppercase and the last word of it transformed into it's plural form
func pluralizeCapitalization(name string) string {
	return capitalize(pluralize(name))
}
