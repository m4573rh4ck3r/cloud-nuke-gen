#!/usr/bin/env bash

set -o pipefail
set -o nounset
set -o errexit

current_tag=$(git describe --tags --abbrev=0)

if [[ "$inc" == "major" ]];
then
	new_tag=$(echo -n $current_tag | sed 's/^v//g' | awk -F. -v OFS=. '{$1++;printf "v%s.0.0\n", $1, $2, $3}')
elif [[ "$inc" == "minor" ]];
then
	new_tag=$(echo -n $current_tag | sed 's/^v//g' | awk -F. -v OFS=. '{$2++;printf "v%s.%s.0\n", $1, $2, $3}')
elif [[ "$inc" == "patch" ]];
then
	new_tag=$(echo -n $current_tag | sed 's/^v//g' | awk -F. -v OFS=. '{$3++;printf "v%s.%s.%s\n", $1, $2, $3}')
fi

git tag $new_tag
